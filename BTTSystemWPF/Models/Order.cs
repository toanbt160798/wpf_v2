﻿using BTTSystemWPF.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemWPF.Models
{
    public partial class Order : INotifyPropertyChanged
    {
        private readonly ProductService productService = new ProductService();
        private readonly UserService userService = new UserService();
        int _ProductId;
        int _UserId;
        string _Description;
        string _ProductName;
        string _Username;
        string _Unit;
        decimal? _Price;
        int? _Quantity;
        decimal? _TotalPrice;
        bool? _IsDelete;
        public int OrderId { get; set; }

        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public bool? IsDelete
        {
            get { return _IsDelete; }
            set
            {
                _IsDelete = value;
                RaisePropertyChanged("IsDelete");
            }
        }
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                if (_ProductId != value)
                {
                    _ProductId = value;
                    RaisePropertyChanged("ProductId");
                    RefreshData(_ProductId);
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }
        public int UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                if (_UserId != value)
                {
                    _UserId = value;
                    RaisePropertyChanged("UserId");
                    RefreshDataUser(_UserId);
                }
            }
        }
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description != value)
                {
                    _Description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }
        public string ProductName
        {
            get
            {
                return _ProductName;
            }
            set
            {
                if (_ProductName != value)
                {
                    _ProductName = value;
                    RaisePropertyChanged("ProductName");
                }
            }
        }
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit != value)
                {
                    _Unit = value;
                    RaisePropertyChanged("Unit");
                }
            }
        }

        public decimal? Price
        {
            get
            {
                return _Price;
            }
            set
            {
                if (_Price != value)
                {
                    _Price = value;
                    RaisePropertyChanged("Price");
                }
            }
        }

        public int? Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                if (_Quantity != value)
                {
                    _Quantity = value;
                    RaisePropertyChanged("Quantity");
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }

        public decimal? TotalPrice
        {
            get
            {
                return _TotalPrice;
            }
            set
            {
                if (_TotalPrice != value)
                {
                    _TotalPrice = value;
                    RaisePropertyChanged("TotalPrice");
                }
            }
        }

        // Private Methods
        private async void RefreshData(int _ProductId)
        {
            // Function call the API to get Product.
            Product product = await productService.GetProduct(_ProductId);
            this.Description = (product == null ? "" : product.Description);
            this.Unit = (product == null ? "" : product.Unit);
            this.Price = (product == null ? 0 : product.Price);
            this.ProductName = (product == null ? "" : product.ProductName);
            
        }
        private async void RefreshDataUser(int _UserId)
        {
            User user = await userService.GetUser(_UserId);
            this.Username = (user == null ? "" : user.Username);
        }
        private void CalculateTotalPrice(decimal? _Price, int? _Quantity)
        {
            this.TotalPrice = _Price * _Quantity;
        }

        internal void RaisePropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
