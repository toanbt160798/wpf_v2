﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using BTTSystemWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BTTSystemWPF.Views.Orders
{
    /// <summary>
    /// Interaction logic for AddOrderView.xaml
    /// </summary>
    public partial class AddOrderView : UserControl
    {

        private readonly OrderService orderService = new OrderService();
        private readonly ProductService productService = new ProductService();
        public List<User> userList { get; set; }
        public ObservableCollection<Order> orderList { get; set; }
        public List<Product> productList { get; set; }
  
        public AddOrderView()
        {
            InitializeComponent();
            SetupOrderDataGrid();
        }
        private async void SetupOrderDataGrid()
        {
            //OrderList = new ObservableCollection<Order>(await orderService.ListOrder());
            // Get product list data from the database.
            orderList = new ObservableCollection<Order>
            {
                new Order()
            };

            // Get product list data from the database.
            productList = await productService.ListProduct();
            DataContext = this;
           
        }
        private void ButtonAddOrder_Click(object sender, RoutedEventArgs e)
        {
            orderList.Add(new Order());
            DataContext = this;
        }

        private void ButtonSubmitOrder_Click(object sender, RoutedEventArgs e)
        {
            AddOrderList();
            
        }
        private async void AddOrderList()
        {
            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to submit these orders?", "Submit Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {

                // Get current user id from App.Current.Properties.
                int userId = int.Parse(Application.Current.Properties["userId"].ToString());

                // Create new dummy list to store data.
                List<Order> submitOrderList = new List<Order>();
                foreach (Order row in dataGridOrderList.Items.OfType<Order>())
                {
                    // Check if user input quantity or not.
                    if (row.Quantity == 0 || row.Quantity == null)
                    {
                        MessageBox.Show("Please enter quantity you want to order.", "Error Message");
                        return;
                    }

                    row.UserId = userId;
                    row.IsDelete = false;
                    submitOrderList.Add(row);
                }

                // Function call the API to add Product and get response data.
                HttpResponseMessage response = await orderService.AddOrderList(submitOrderList);
                var responseData = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
                    mainWindow.mainContent.Content = new OrderViewModel();
                }
            }
        }
    }
}
