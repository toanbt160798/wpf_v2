﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using BTTSystemWPF.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BTTSystemWPF.Views.Orders
{
    /// <summary>
    /// Interaction logic for OrderView.xaml
    /// </summary>
    public partial class OrderView : UserControl
    {
        private readonly OrderService orderService = new OrderService();
        private readonly ProductService productService = new ProductService();
        private readonly UserService userService = new UserService();
        public ObservableCollection<Order> orderList { get; set; }
        public List<Product> productList { get; set; }
        public List<User> userList { get; set; }
        public OrderView()
        {
            InitializeComponent();
            SetupOrderDataGrid();
        }

        private void ButtonAddOrder_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
            mainWindow.mainContent.Content = new AddOrderView();
            // Create 1 new row of order list.
            //orderList.Add(new Order());

            // Set data grid's data.
            //DataContext = this;
        }
        
        private async void SetupOrderDataGrid()
        {
            // Create 1 new row of order list.
            orderList = new ObservableCollection<Order>(await orderService.ListOrder());
           
            productList = await productService.ListProduct();
            userList = await userService.ListUser();
            // Set data grid's data.
            DataContext = this;
        }
        
    }
}
