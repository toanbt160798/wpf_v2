﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using BTTSystemWPF.Views.Products;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;

namespace BTTSystemWPF.Views.Products
{
    /// <summary>
    /// Interaction logic for ProductView.xaml
    /// </summary>
    public partial class ProductView : UserControl
    {
        private readonly ProductService productService = new ProductService();

        public ProductView()
        {
            InitializeComponent();

            // Get product list.
            ListProduct();
        }
        private async void ListProduct()
        {
            // Get product list.
            dataGridProductList.ItemsSource = await productService.ListProduct();
        }

        private void ButtonAddProduct_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
            mainWindow.mainContent.Content = new AddProductView();
        }

        private void ButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            Product product = (Product)((Button)e.Source).DataContext;
            int productId = product.ProductId;
            Application.Current.Properties["productId"] = productId;
            MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
            mainWindow.mainContent.Content = new EditProductView();
        }

        private void ButtonDelete_Click(object sender, RoutedEventArgs e)
        {
            Product product = (Product)((Button)e.Source).DataContext;
            int productId = product.ProductId;

            // Set productId into App.Current.Properties. This treats productId like a global variable.
            Application.Current.Properties["productId"] = productId;

            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Product?", "Delete Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Delete the selected Product.
                DeleteProduct();
            }
        }
        private async void DeleteProduct()
        {
            // Get data from the App.Current.Properties. These work like global variables.
            int productId = int.Parse(Application.Current.Properties["productId"].ToString());

            // Completing the URL and call the API to get response data.
            HttpResponseMessage response = await productService.DeleteProduct(productId);

            if (response.IsSuccessStatusCode)
            {
                // Refresh the data grid.
                ListProduct();
            }
        }
    }
}
