﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using BTTSystemWPF.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BTTSystemWPF.Views.Products
{
    /// <summary>
    /// Interaction logic for AddProductView.xaml
    /// </summary>
    public partial class AddProductView : UserControl
    {
        private readonly ProductService productService = new ProductService();
        public AddProductView()
        {
            InitializeComponent();
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            AddProduct();
        }
        private void Reset()
        {
            // Clear all input fields.
            txbProduct.Text = "";
            txbDescription.Text = "";
            txbPrice.Text = "";
            txbUint.Text = "";
            textBlockResponseMessage.Text = "";
        }
        private async void AddProduct()
        {
            string returnMessage = "";
            bool returnFlag = false;
            if (string.IsNullOrEmpty(txbProduct.Text))
            {
                returnMessage += Environment.NewLine + "- Product must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbUint.Text))
            {
                returnMessage += Environment.NewLine + "- Unit must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbDescription.Text))
            {
                returnMessage += Environment.NewLine + "- Description must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbPrice.Text))
            {
                returnMessage += Environment.NewLine + "- Price must not be empty.";
                returnFlag = true;
            }
            if (returnFlag)
            {
                MessageBox.Show(returnMessage, "Validation Message");
            }
            else
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to add this product?", "Save Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    // Create new Product model to push it to the AddProduct function.
                    Product product = new Product()
                    {
                        ProductName = txbProduct.Text,
                        Description = txbDescription.Text,
                        Price = decimal.Parse(txbPrice.Text),
                        Unit = txbUint.Text,
                        IsDelete = false
                    };

                    // Function call the API to add Product and get response data.
                    var response = await productService.AddProduct(product);
                    var responseData = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        MainWindow mainWindow = (MainWindow)MainWindow.GetWindow(this);
                        mainWindow.mainContent.Content = new ProductView();
                    }
                    else
                    {
                        // Change response message text to Red.
                        textBlockResponseMessage.Foreground = Brushes.Red;
                    }

                    // Show the response message from the server.
                    textBlockResponseMessage.Text = responseData;
                }
            }

        }
        private void TxbPrice_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
