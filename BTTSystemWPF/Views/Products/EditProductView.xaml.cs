﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using BTTSystemWPF.ViewModels;
using BTTSystemWPF.Views.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BTTSystemWPF.Views
{
    /// <summary>
    /// Interaction logic for EditProductView.xaml
    /// </summary>
    public partial class EditProductView : UserControl
    {
        private readonly ProductService productService = new ProductService();

        public EditProductView()
        {
            InitializeComponent();
            GetProduct();
        }

        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            EditProduct();
        }

        private void ButtonReset_Click(object sender, RoutedEventArgs e)
        {
            // Yes/No confirmation box.
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to reset?", "Reset Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Clear all input fields.
                Reset();
            }
        }
        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainWindow mainWindow = (MainWindow)MainWindow.GetWindow(this);
            mainWindow.mainContent.Content = new ProductView();
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            EditProduct();
        }
        private void Reset()
        {
            // Clear all input fields.
            txbProduct.Text = "";
            txbDescription.Text = "";
            txbPrice.Text = "";
            txbUint.Text = "";
            textBlockResponseMessage.Text = "";
        }

        private async void GetProduct()
        {
            // Get data from the App.Current.Properties. These work like global variables.
            int productId = int.Parse(Application.Current.Properties["productId"].ToString());

            // Function call the API to get Product and get response data.
            Product product = await productService.GetProduct(productId);

            // Assign it to the view.
            txbProduct.Text = product.ProductName;
            txbDescription.Text = product.Description;
            txbPrice.Text = product.Price.ToString();
            txbUint.Text = product.Unit;
        }

        private async void EditProduct()
        {
            string returnMessage = "";
            bool returnFlag = false;
            if (string.IsNullOrEmpty(txbProduct.Text))
            {
                returnMessage += Environment.NewLine + "- Product must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbUint.Text))
            {
                returnMessage += Environment.NewLine + "- Unit must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbDescription.Text))
            {
                returnMessage += Environment.NewLine + "- Description must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(txbPrice.Text))
            {
                returnMessage += Environment.NewLine + "- Price must not be empty.";
                returnFlag = true;
            }
            if (returnFlag)
            {
                MessageBox.Show(returnMessage, "Validation Message");
            }
            else
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to edit this product?", "Edit Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    // Get data from the App.Current.Properties. These work like global variables.
                    int productId = int.Parse(Application.Current.Properties["productId"].ToString());

                    // Get input data from current view.
                    string productName = txbProduct.Text;
                    string description = txbDescription.Text;
                    double price = double.Parse(txbPrice.Text);
                    string unit = txbUint.Text;

                    // Create new Product model to push it to the EditProduct function.
                    Product product = new Product()
                    {
                        ProductId = productId,
                        ProductName = productName,
                        Description = description,
                        Price = (decimal)price,
                        IsDelete = false,
                        Unit = unit
                    };

                    // Function call the API to add Product and get response data.
                    var response = await productService.EditProduct(product);
                    var responseData = response.Content.ReadAsStringAsync().Result;

                    if (response.IsSuccessStatusCode)
                    {
                        // Remove the productId set in App.Current.Properties.
                        Application.Current.Properties["productId"] = null;

                        // Navigate from this view to another view.
                        MainWindow mainWindow = (MainWindow)Window.GetWindow(this);
                        mainWindow.mainContent.Content = new ProductViewModel();
                    }
                    else
                    {
                        // Change response message text to Red.
                        textBlockResponseMessage.Foreground = Brushes.Red;
                    }

                    // Show the response message from the server.
                    textBlockResponseMessage.Text = responseData;
                }
            }
            
        }
    }
}
