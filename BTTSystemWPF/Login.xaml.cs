﻿using BTTSystemWPF.Models;
using BTTSystemWPF.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BTTSystemWPF
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        private readonly UserService userService = new UserService();
        public Login()
        {
            InitializeComponent();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            StartLogin();
        }
        private async void StartLogin()
        {
            // Get input data.
            string username = textBoxUsername.Text;
            string password = passwordBoxLogin.Password;

            // Completing the URL and call the API to get response data.
            HttpResponseMessage response = await userService.Login(username, password);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                int userId = int.Parse(responseData);
                Application.Current.Properties["userId"] = userId;
                // Show the Main Window.
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();

                // Close the Login Window.
                Close();
            }
        }
    }
}
