﻿using BTTSystemWPF.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BTTSystemWPF.Services
{
    public class UserService
    {
        private readonly CommonService commonServices = new CommonService();
        public async Task<List<User>> ListUser()
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = await commonServices.HttpRequest("GET", "/users", null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<List<User>>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }
        public async Task<HttpResponseMessage> Login(string username, string password)
        {
            // Create a JSON body to send with the request to the API.
            string jsonBody = "{'Username': '" + username + "', 'Password': '" + password + "'}";

            // Create API URL and call it to get response with json body (pass "LOGIN" string as method to check).
            return await commonServices.HttpRequest("POST", "/login", jsonBody);
        }
        public async Task<User> GetUser(int userId)
        {
            // Create API URL and call it to get response without json body (get method).
            HttpResponseMessage response = await commonServices.HttpRequest("GET", "/user/" + userId, null);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the response data and then return.
                return JsonConvert.DeserializeObject<User>(responseData);
            }

            MessageBox.Show(responseData, "Response Message");

            return null;
        }
    }
}
