﻿using BTTSystemWPF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemWPF.ViewModels
{
    public class OrderViewModel
    {
        public int _orderId;
        public int OrderId
        {
            get { return _orderId; }
            set { _orderId = value; }
        }
        public int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public int _userId;
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public int _quantity;
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        public bool? _isdelete;
        public bool? IsDelete
        {
            get { return _isdelete; }
            set { _isdelete = value; }
        }
        public Product _product;
        public Product Product
        {
            get { return _product; }
            set { _product = value; }
        }
    }
}
