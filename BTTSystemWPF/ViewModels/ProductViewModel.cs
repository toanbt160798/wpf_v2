﻿using BTTSystemWPF.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemWPF.ViewModels
{
    public class ProductViewModel
    {
        public int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }
        public string _productName;
        public string ProductName
        {
            get { return _productName; }
            set { _productName = value; }
        }
        public string _description;
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public decimal? _price;
        public decimal? Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public string _unit;
        public string Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }
        public bool? _isdelete;

        public bool? IsDelete
        {
            get { return _isdelete; }
            set { _isdelete = value; }
        }
    }
}
