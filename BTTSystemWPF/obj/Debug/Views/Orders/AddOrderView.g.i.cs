﻿#pragma checksum "..\..\..\..\Views\Orders\AddOrderView.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "815A81619EE63A0393060FEE5A3465344B474F580DEF5D599E85DA0AF1F1A09B"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BTTSystemWPF.Views.Orders;
using DevExpress.Xpf.DXBinding;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BTTSystemWPF.Views.Orders {
    
    
    /// <summary>
    /// AddOrderView
    /// </summary>
    public partial class AddOrderView : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\..\..\Views\Orders\AddOrderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonAddOrder;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\..\..\Views\Orders\AddOrderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button buttonSubmitOrder;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\Views\Orders\AddOrderView.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dataGridOrderList;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BTTSystemWPF;component/views/orders/addorderview.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Views\Orders\AddOrderView.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.buttonAddOrder = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\..\..\Views\Orders\AddOrderView.xaml"
            this.buttonAddOrder.Click += new System.Windows.RoutedEventHandler(this.ButtonAddOrder_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.buttonSubmitOrder = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\..\..\Views\Orders\AddOrderView.xaml"
            this.buttonSubmitOrder.Click += new System.Windows.RoutedEventHandler(this.ButtonSubmitOrder_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.dataGridOrderList = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

