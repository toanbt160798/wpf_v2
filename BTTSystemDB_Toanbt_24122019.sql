USE [BTTSystemDB]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 12/24/2019 09:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](100) NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Users] ON
INSERT [dbo].[Users] ([UserId], [UserName], [Password], [IsDelete]) VALUES (1, N'u1', N'202CB962AC59075B964B07152D234B70', 0)
INSERT [dbo].[Users] ([UserId], [UserName], [Password], [IsDelete]) VALUES (2, N'u2', N'202CB962AC59075B964B07152D234B70', 0)
SET IDENTITY_INSERT [dbo].[Users] OFF
/****** Object:  Table [dbo].[Products]    Script Date: 12/24/2019 09:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NULL,
	[Description] [text] NULL,
	[Price] [decimal](18, 2) NULL,
	[Unit] [nvarchar](20) NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Products] ON
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (1, N'Product 01', N'PXX-01', CAST(2000.00 AS Decimal(18, 2)), N'Can', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (2, N'Product 02', N'PR-02', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (3, N'Product 03', N'PR-03', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (4, N'Product 04', N'PR-04', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (5, N'Product 05', N'PR-05', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (6, N'Product 06', N'PR-06', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (7, N'Product 07', N'PR-07', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (8, N'Product 08', N'PR-08', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (9, N'Product 09', N'PR-09', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (10, N'Product 10', N'PR-10', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (11, N'Product 11', N'PR-11', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (12, N'Product 12', N'PR-12', CAST(2100.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (13, N'Product 13', N'PR-13', CAST(3.20 AS Decimal(18, 2)), N'Can', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (14, N'Demo 01', N'Demo 01', CAST(2.50 AS Decimal(18, 2)), N'Can', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (15, N'Demo 02', N'Demo 02', CAST(2.23 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (16, N'Demo 03', N'Demo 03', CAST(333.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (17, N'Demo 04', N'Demo 04', CAST(322.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (18, N'Demo 05', N'Demo 05', CAST(43001.00 AS Decimal(18, 2)), N'Box', 0)
INSERT [dbo].[Products] ([ProductId], [ProductName], [Description], [Price], [Unit], [IsDelete]) VALUES (19, N'Demo 06', N'Demo 06', CAST(9.00 AS Decimal(18, 2)), N'Box', 0)
SET IDENTITY_INSERT [dbo].[Products] OFF
/****** Object:  Table [dbo].[Orders]    Script Date: 12/24/2019 09:47:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[OrderId] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NULL,
	[UserId] [int] NULL,
	[Quantity] [int] NULL,
	[IsDelete] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Orders] ON
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (1, 2, 1, 5, 1)
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (2, 1, 1, 5, 0)
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (3, 19, 2, 1, 1)
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (4, 18, 2, 1, 1)
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (5, 12, 2, 1, 0)
INSERT [dbo].[Orders] ([OrderId], [ProductId], [UserId], [Quantity], [IsDelete]) VALUES (6, 3, 2, 1, 1)
SET IDENTITY_INSERT [dbo].[Orders] OFF
/****** Object:  ForeignKey [FK__Orders__ProductI__24927208]    Script Date: 12/24/2019 09:47:01 ******/
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
/****** Object:  ForeignKey [FK__Orders__UserId__25869641]    Script Date: 12/24/2019 09:47:01 ******/
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([UserId])
GO
