﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BTTSystemMVVM.Services
{
    public partial class CommonService
    {
        public string GetApiUrl()
        {
            // Get API URL from the App.config file.
            return ConfigurationManager.AppSettings["BTTSystemAPI"];
        }

        public string GetToken()
        {
            // Get token data from the App.Current.Properties. This works like a global variable.
            return Application.Current.Properties["token"].ToString();
        }

        public async Task<HttpResponseMessage> HttpRequest(string method, string apiControllerUrl, string jsonBody)
        {
            // Get API URL from the App.config file.
            string apiUrl = GetApiUrl();

            // HTTP declarations and settings to connect with the API.
            HttpClient httpClient = new HttpClient();

            HttpResponseMessage response = null;

            // Check method and call API accordingly.
            if (string.Equals(method, "GET", StringComparison.OrdinalIgnoreCase))
            {
                response = httpClient.GetAsync(apiUrl + apiControllerUrl).Result;
            }
            else if (string.Equals(method, "POST", StringComparison.OrdinalIgnoreCase))
            {
                response = await httpClient.PostAsync(apiUrl + apiControllerUrl,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));
            }
            else if (string.Equals(method, "PUT", StringComparison.OrdinalIgnoreCase))
            {
                response = await httpClient.PutAsync(apiUrl + apiControllerUrl,
                    new StringContent(jsonBody, Encoding.UTF8, "application/json"));
            }
            else if (string.Equals(method, "DELETE", StringComparison.OrdinalIgnoreCase))
            {
                response = httpClient.DeleteAsync(apiUrl + apiControllerUrl).Result;
            }

            return response;
        }
    }
}
