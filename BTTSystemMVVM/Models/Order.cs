﻿using BTTSystemMVVM.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BTTSystemMVVM.Models
{
    public partial class Order : INotifyPropertyChanged
    {
        private readonly ProductService productService = new ProductService();
        private readonly UserService userService = new UserService();
        int _ProductId;
        int _UserId;
        string _Description;
        string _ProductName;
        string _Username;
        string _Unit;
        double? _Price;
        int? _Quantity;
        double? _TotalPrice;
        bool? _IsDelete;
        public int OrderId { get; set; }
        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public bool? IsDelete
        {
            get { return _IsDelete; }
            set
            {
                _IsDelete = value;
                OnPropertyChanged("IsDelete");
            }
        }
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                if (_ProductId != value)
                {
                    _ProductId = value;
                    OnPropertyChanged("ProductId");
                    RefreshData(_ProductId);
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }
        public int UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                if (_UserId != value)
                {
                    _UserId = value;
                    OnPropertyChanged("UserId");
                    RefreshDataUser(_UserId);
                }
            }
        }
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description != value)
                {
                    _Description = value;
                    OnPropertyChanged("Description");
                }
            }
        }
        public string ProductName
        {
            get
            {
                return _ProductName;
            }
            set
            {
                if (_ProductName != value)
                {
                    _ProductName = value;
                    OnPropertyChanged("ProductName");
                }
            }
        }
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    OnPropertyChanged("Username");
                }
            }
        }
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit != value)
                {
                    _Unit = value;
                    OnPropertyChanged("Unit");
                }
            }
        }
        public double? Price
        {
            get
            {
                return _Price;
            }
            set
            {
                if (_Price != value)
                {
                    _Price = value;
                    OnPropertyChanged("Price");
                }
            }
        }
        public int? Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                if (_Quantity != value)
                {
                    _Quantity = value;
                    OnPropertyChanged("Quantity");
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }
        public double? TotalPrice
        {
            get
            {
                return _TotalPrice;
            }
            set
            {
                if (_TotalPrice != value)
                {
                    _TotalPrice = value;
                    OnPropertyChanged("TotalPrice");
                }
            }
        }
        // Private Methods
        private async void RefreshData(int _ProductId)
        {
            // Function call the API to get Product.
            Product product = await productService.GetProduct(_ProductId);
            this.Description = (product == null ? "" : product.Description);
            this.Unit = (product == null ? "" : product.Unit);
            this.Price = (product == null ? 0 : product.Price);
            this.ProductName = (product == null ? "" : product.ProductName);
        }
        private async void RefreshDataUser(int _UserId)
        {
            User user = await userService.GetUser(_UserId);
            this.Username = (user == null ? "" : user.Username);
        }
        private void CalculateTotalPrice(double? _Price, int? _Quantity)
        {
            this.TotalPrice = _Price * _Quantity;
        }
        internal void OnPropertyChanged(string prop)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
