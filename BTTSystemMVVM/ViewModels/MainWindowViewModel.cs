﻿using BTTSystemMVVM.Models;
using BTTSystemMVVM.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BTTSystemMVVM.ViewModels
{
    class MainWindowViewModel : BaseViewModel
    {
        public bool Isloaded = false;
        private string _Username;
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    OnPropertyChanged("Username");
                }
            }
        }
        public ICommand LoadedWindowCommand { get; set; }
        public ICommand ShowProductCommand { get; set; }
        public ICommand ShowOrderCommand { get; set; }
        
        public MainWindowViewModel()
        {
            LoadedWindowCommand = new RelayCommand<Window>((p) => { return true; }, (p) => {
                Isloaded = true;
                if (p == null)
                    return;
                p.Hide();
                LoginWindow loginWindow = new LoginWindow();
                loginWindow.ShowDialog();

                if (loginWindow.DataContext == null)
                    return;
                var loginVM = loginWindow.DataContext as LoginViewModel;
                Username = loginVM.Username;
                if (loginVM.IsLogin)
                {
                    p.Show();
                }
                else
                {
                    p.Close();
                }
            });
            ShowProductCommand = new RelayCommand<object>((p) => { return true; }, (p) => { ProductWindow pw = new ProductWindow(); pw.ShowDialog(); });
            ShowOrderCommand = new RelayCommand<object>((p) => { return true; }, (p) => { OrderWindow ow = new OrderWindow(); ow.ShowDialog(); });
        }

    }
}
