﻿using BTTSystemMVVM.Models;
using BTTSystemMVVM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BTTSystemMVVM.ViewModels
{
   public class ProductViewModel : BaseViewModel
   {
        private readonly ProductService _ProductService = new ProductService();
        private readonly OrderService _OrderService = new OrderService();
        private ObservableCollection<Product> _ProductList;
        public ObservableCollection<Product> ProductList
        {
            get
            {
                return _ProductList;
            }
            set
            {
                if (_ProductList != value)
                {
                    _ProductList = value;
                    OnPropertyChanged("ProductList");
                }
            }
        }
        public Product _SelectedItem;
        public Product SelectedItem
        {
            get => _SelectedItem;
            set
            {
                _SelectedItem = value;
                OnPropertyChanged();
                if (SelectedItem !=null)
                {
                    ProductId = SelectedItem.ProductId;
                    ProductName = SelectedItem.ProductName;
                    Description = SelectedItem.Description;
                    Unit = SelectedItem.Unit;
                    Price = SelectedItem.Price;
                }
            }
        }
        public int _ProductId;
        public int ProductId
        {
            get { return _ProductId; }
            set
            {
                _ProductId = value;
                OnPropertyChanged("ProductId");
            }
        }
        public string _ProductName;
        public string ProductName
        {
            get
            {
                return _ProductName;
            }
            set
            {
                if (_ProductName != value)
                {
                    _ProductName = value;
                    OnPropertyChanged("ProductName");
                }
            }
        }
        public string _Description;
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description != value)
                {
                    _Description = value;
                    OnPropertyChanged("Description");
                }
            }
        }

        public string _Unit;
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit != value)
                {
                    _Unit = value;
                    OnPropertyChanged("Unit");
                }
            }
        }

        public double? _Price;
        public double? Price
        {
            get
            {
                return _Price;
            }
            set
            {
                if (_Price != value)
                {
                    _Price = value;                    
                    OnPropertyChanged("Price");
                }
            }
        }
        public ICommand AddProductCommand { get; set; }
        public ICommand EditProductCommand { get; set; }
        public ICommand DeleteProductCommand { get; set; }
        public ProductViewModel()
        {
            GetProducts();
            AddProductCommand = new RelayCommand<object>((p) =>
                {
                    return true;
                }, (p) =>
                {
                    Add();
                }
            );
            EditProductCommand = new RelayCommand<object>((p) =>
            {
                return true;
            },(p) =>{
                Update();
            });
            DeleteProductCommand = new RelayCommand<object>((p) =>
            {
                return true;
            }, (p) => {
                DeleteProduct();
            });
        }
        private async void Add()
        {
            string returnMessage = "";
            bool returnFlag = false;
            if (string.IsNullOrEmpty(ProductName))
            {
                returnMessage += Environment.NewLine + "- Product must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Unit))
            {
                returnMessage += Environment.NewLine + "- Unit must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Description))
            {
                returnMessage += Environment.NewLine + "- Description must not be empty.";
                returnFlag = true;
            }
            if (string.IsNullOrEmpty(Price.ToString()))
            {
                returnMessage += Environment.NewLine + "- Price must not be empty.";
                returnFlag = true;
            }
            if (returnFlag)
            {
                MessageBox.Show(returnMessage, "Validation Message");
            }
            else
            {
                var product = new Product()
                {
                    ProductName = ProductName,
                    Description = Description,
                    Price = Price,
                    Unit = Unit,
                    IsDelete = false,
                };
                var response = await _ProductService.AddProduct(product);
                if (response.IsSuccessStatusCode)
                {
                    GetProducts();
                    ResetProduct();
                }
            }            
        }
        private async void Update()
        {
            Product product = await _ProductService.GetProduct(ProductId);
            if (product != null)
            {
                string returnMessage = "";
                bool returnFlag = false;
                if (string.IsNullOrEmpty(ProductName))
                {
                    returnMessage += Environment.NewLine + "- Product must not be empty.";
                    returnFlag = true;
                }
                if (string.IsNullOrEmpty(Unit))
                {
                    returnMessage += Environment.NewLine + "- Unit must not be empty.";
                    returnFlag = true;
                }
                if (string.IsNullOrEmpty(Description))
                {
                    returnMessage += Environment.NewLine + "- Description must not be empty.";
                    returnFlag = true;
                }
                if (string.IsNullOrEmpty(Price.ToString()))
                {
                    returnMessage += Environment.NewLine + "- Price must not be empty.";
                    returnFlag = true;
                }
                if (returnFlag)
                {
                    MessageBox.Show(returnMessage, "Validation Message");
                }
                else
                {
                    product.ProductName = ProductName;
                    product.Description = Description;
                    product.Price = Price;
                    product.Unit = Unit;
                    product.IsDelete = false;
                    var response = await _ProductService.EditProduct(product);
                    if (response.IsSuccessStatusCode)
                    {
                        GetProducts();
                        ResetProduct();
                    }
                }
            }            
        }
        private async void DeleteProduct()
        {
            Product product = await _ProductService.GetProduct(ProductId);
            List<Order> order = await _OrderService.ListOrder();
            bool check = false;
            foreach (var item in order)
            {
                if (item.ProductId == ProductId)
                {
                    check = true;                 
                }
            }
            if (check)
            {
                MessageBox.Show("This record is being used in another place!", "Message erro");
            }
            else if (product != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Product?", "Delete Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    HttpResponseMessage response = await _ProductService.DeleteProduct(ProductId);

                    if (response.IsSuccessStatusCode)
                    {
                        GetProducts();
                        ResetProduct();
                    }
                }
            }            
        }
        private void ResetProduct()
        {
            // Clear all input fields.
            SelectedItem = new Product();
        }
        private async void GetProducts()
        {
            // Get product list.            
            ProductList = new ObservableCollection<Product>(await _ProductService.ListProduct());
           
        }
    }
}
