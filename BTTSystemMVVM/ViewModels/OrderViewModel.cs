﻿using BTTSystemMVVM.Models;
using BTTSystemMVVM.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace BTTSystemMVVM.ViewModels
{
    public class OrderViewModel : BaseViewModel
    {
        public ObservableCollection<Order> _OrderList;
        public ObservableCollection<Order> _Orders;
        public static List<Product> _ProductList;
        public static int? _AllQuantity;
        public static decimal? _AllPrice;
        private readonly OrderService _OrderService = new OrderService();
        private readonly ProductService _ProductService = new ProductService();
        public Order _SelectedItem;
        public Order SelectedItem
        {
            get {return _SelectedItem; }
            set
            {
                _SelectedItem = value;
                OnPropertyChanged("SelectedItem");
            }
        }
        public ObservableCollection<Order> OrderList
        {
            get
            {
                return _OrderList;
            }
            set
            {
                if (_OrderList != value)
                {
                    _OrderList = value;
                    OnPropertyChanged("OrderList");
                }
            }
        }
        public ObservableCollection<Order> Orders
        {
            get
            {
                return _Orders;
            }
            set
            {
                if (_Orders != value)
                {
                    _Orders = value;
                    OnPropertyChanged("Orders");
                }
            }
        }
        public List<Product> ProductList
        {
            get
            {
                return _ProductList;
            }
            set
            {
                if (_ProductList != value)
                {
                    _ProductList = value;
                    OnPropertyChanged("ProductList");
                }
            }
        }
        public ICommand AddOrderCommand { get; set; }
        public ICommand SaveOrderCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public OrderViewModel()
        {
            SetupOrderDataGrid();
            AddOrderCommand = new RelayCommand<object>(
                (p) => { return true; }, 
                (p) => {
                    Orders.Add(new Order());
                }
            );
            DeleteCommand = new RelayCommand<object>(
                (p) => { return true; }, 
                (p) => {
                    Orders.Remove(SelectedItem);
                }
            );
            SaveOrderCommand = new RelayCommand<object>((p) => { return true; }, (p) => { AddOrderList(); });
        }
        private async void SetupOrderDataGrid()
        {
            // Create 1 new row of order list.
            OrderList = new ObservableCollection<Order>(await _OrderService.ListOrder());
            Orders = new ObservableCollection<Order>
            {
                new Order()
            };
            // Get product list data from the database.
            ProductList = await _ProductService.ListProduct();
        }
        private async void AddOrderList()
        {

            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to submit these orders?", "Submit Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Get current user id from App.Current.Properties.
                int userId = int.Parse(Application.Current.Properties["userId"].ToString());
                List<Order> submitOrderList = new List<Order>();

                Order submitOrder;

                foreach (Order order in _Orders)
                {
                    submitOrder = new Order();
                    if (order.ProductId == 0)
                    {
                        MessageBox.Show("Please choose a product.", "Error Message");
                        return;
                    }
                    if (order.Quantity == 0 || order.Quantity == null)
                    {
                        MessageBox.Show("Please enter quantity you want to order.", "Error Message");
                        return;
                    }
                    submitOrder.Quantity = order.Quantity;
                    submitOrder.ProductId = order.ProductId;
                    submitOrder.IsDelete = false;
                    submitOrder.UserId = userId;
                    submitOrderList.Add(submitOrder);
                }
                // Function call the API to add Product and get response data.
                HttpResponseMessage response = await _OrderService.AddOrderList(submitOrderList);
                var responseData = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    SetupOrderDataGrid();
                    ResetOrder();
                }
            }
        }
        private void ResetOrder()
        {
            Orders = new ObservableCollection<Order>
            {
                new Order()
            };
        }
        public class NumberConverter : IValueConverter
        {
            public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                return value.ToString();
            }
            public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                var input = value as string;
                int output;
                if (int.TryParse(input, out output))
                    return output;
                else
                    return DependencyProperty.UnsetValue;
            }
        }
    }
}
