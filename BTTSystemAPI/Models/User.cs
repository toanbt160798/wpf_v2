﻿using System;
using System.Collections.Generic;

namespace BTTSystemAPI.Models
{
    public partial class Users
    {
        public Users()
        {
            Orders = new HashSet<Orders>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
