﻿using System;
using System.Collections.Generic;

namespace BTTSystemAPI.Models
{
    public partial class Products
    {
        public Products()
        {
            Orders = new HashSet<Orders>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public float? Price { get; set; }
        public string Unit { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Orders> Orders { get; set; }
    }
}
