﻿using System;
using System.Collections.Generic;

namespace BTTSystemAPI.Models
{
    public partial class Orders
    {
        public int OrderId { get; set; }
        public int? ProductId { get; set; }
        public int? UserId { get; set; }
        public int? Quantity { get; set; }
        public bool? IsDelete { get; set; }

        public virtual Products Product { get; set; }
        public virtual Users User { get; set; }
    }
}
