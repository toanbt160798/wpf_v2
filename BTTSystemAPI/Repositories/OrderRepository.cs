﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        protected readonly BTTSystemDBContext _dataContext;
        public OrderRepository(BTTSystemDBContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<int> AddOrder(Orders order)
        {
            int result = 0;
            if (_dataContext != null)
            {
                // Add that Order.
                await _dataContext.Orders.AddAsync(order);

                // Commit the transaction.
                await _dataContext.SaveChangesAsync();

                result = order.OrderId;
            }
            return result;
        }
        public void DeleteOrder(Orders order)
        {
            _dataContext.Set<Orders>().Update(order);
            _dataContext.SaveChanges();           
        }

        public Orders Get(int id)
        {
            return _dataContext.Set<Orders>().Find(id);
        }

        public async Task<List<Orders>> ListOrder()
        {

            if (_dataContext != null)
            {
                return await (_dataContext.Orders.OrderByDescending(o => o.OrderId).Where(p => p.IsDelete == false).ToListAsync());
            }
            return null;
        }
    }
}
