﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly BTTSystemDBContext _context;
        public UserRepository(BTTSystemDBContext context)
        {
            _context = context;
        }
        public IEnumerable<Users> GetAll()
        {
            return _context.Users.Where(p => p.IsDelete == false).ToList();
        }
        public Users Get(int id)
        {
            return _context.Set<Users>().Find(id);
        }
        public async Task<Users> CheckLogin(string username, string password)
        {
            if (_context != null)
            {
                return await (from u in _context.Users
                              where String.Compare(u.UserName, username, false) == 0
                              && u.Password == CreateMD5(password) && u.IsDelete == false
                              select new Users
                              {
                                  UserId = u.UserId,
                                  UserName = u.UserName,
                                  Password = u.Password
                              }).FirstOrDefaultAsync();
            }
            return null;
        }

        public void Add(Users user)
        {
            string pass = CreateMD5(user.Password);
            user.Password = pass;
            _context.Set<Users>().Add(user);
            _context.SaveChanges();
        }
        public void Update(Users user)
        {
            string pass = CreateMD5(user.Password);
            user.Password = pass;
            _context.Set<Users>().Update(user);
            _context.SaveChanges();
        }
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
