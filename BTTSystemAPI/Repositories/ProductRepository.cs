﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories
{
    public class ProductRepository : IProductRepository
    {
        protected readonly BTTSystemDBContext _dataContext;
        public ProductRepository(BTTSystemDBContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Add(Products product)
        {
            _dataContext.Set<Products>().Add(product);
            _dataContext.SaveChanges();
        }

        public void Delete(Products product)
        {
            var order =  _dataContext.Orders.Where(p => p.ProductId == product.ProductId && p.IsDelete == false).ToList();
            if(order.Count() == 0)
            {
                _dataContext.Set<Products>().Update(product);
                _dataContext.SaveChanges();
            }
            else
            {
                return;
            }
        }

        public Products Get(int id)
        {
            return _dataContext.Set<Products>().Find(id);
        }

        public IEnumerable<Products> GetAll()
        {
            return  _dataContext.Products.OrderByDescending(p => p.ProductId).Where(p => p.IsDelete == false).ToList();
        }

        public void Update(Products product)
        {
            _dataContext.Set<Products>().Update(product);
            _dataContext.SaveChanges();
        }
    }
}
