﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories
{
    public class DataRepository<TEntity> : IDataRepository<TEntity> where TEntity : class
    {
        protected readonly BTTSystemDBContext _dataContext;
        public DataRepository(BTTSystemDBContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void Add(TEntity entity)
        {
            _dataContext.Set<TEntity>().Add(entity);
            _dataContext.SaveChanges();
        }

        public void Delete(TEntity entity)
        {
            _dataContext.Set<TEntity>().Update(entity);
            _dataContext.SaveChanges();
        }

        public TEntity Get(int id)
        {
            return _dataContext.Set<TEntity>().Find(id);
        }
        
        public void Update(TEntity entity)
        {
            _dataContext.Set<TEntity>().Update(entity);
            _dataContext.SaveChanges();
        }
    }
}
