﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories.Interfaces
{
    public interface IDataRepository<TEntity> where TEntity : class
    {
        
        TEntity Get(int id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
    }
}
