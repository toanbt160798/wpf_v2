﻿using BTTSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        Orders Get(int id);
        Task<int> AddOrder(Orders order);
        void DeleteOrder(Orders order);
        Task<List<Orders>> ListOrder();
    }
}
