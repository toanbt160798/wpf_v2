﻿using BTTSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<Users> CheckLogin(string username, string password);
        IEnumerable<Users> GetAll();
        Users Get(int id);
        void Add(Users user);
        void Update(Users user);
    }
}
