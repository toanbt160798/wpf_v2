﻿using BTTSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Repositories.Interfaces
{
    public interface IProductRepository
    {
        IEnumerable<Products> GetAll();
        Products Get(int id);
        void Add(Products product);
        void Update(Products product);
        void Delete(Products product);
    }
}
