﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BTTSystemAPI.Models;
using BTTSystemAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BTTSystemAPI.Controllers
{

    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }
        [HttpGet]
        [Route("api/products")]
        public IActionResult Get()
        {
            IEnumerable<Products> products = _productService.GetAll();
            return Ok(products);
        }
        [HttpGet]
        [Route("api/product/{id}")]
        public IActionResult Get(int id)
        {
            Products product = _productService.Get(id);

            if (product == null)
            {
                return NotFound("The Product record couldn't be found.");
            }

            return Ok(product);
        }
        [HttpPost]
        [Route("api/product")]
        public IActionResult Add([FromBody] Products product)
        {
            if (product == null)
            {
                return BadRequest("Product is null.");
            }

            _productService.Add(product);
            return StatusCode(200, "Product added sucessfully.");
        }
        [HttpPut]
        [Route("api/product/{id}")]
        public IActionResult Update(int id, [FromBody] Products product)
        {
            if (product == null)
            {
                return BadRequest("Product is null.");
            }

            Products productToUpdate = product;
            if (productToUpdate == null)
            {
                return NotFound("The Product record couldn't be found.");
            }
            else
            {
                product.ProductId = id;
                _productService.Update(productToUpdate);
                return StatusCode(200, "Product updated sucessfully.");
            }
        }
        [HttpDelete]
        [Route("api/product/{id}")]
        public IActionResult Delete(int id)
        {
            Products product = _productService.Get(id);
            if (product == null)
            {
                return NotFound("The Product record couldn't be found.");
            }
            product.IsDelete = true;
            _productService.Delete(product);
            return StatusCode(200, "Product deleted sucessfully.");
        }
    }
}