﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BTTSystemAPI.Models;
using BTTSystemAPI.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BTTSystemAPI.Controllers
{
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;
        private readonly IUserService _userService;
        public OrderController(IOrderService  orderService)
        {
            _orderService = orderService;
        }
        [HttpGet]
        [Route("api/orders")]
        public async Task<IActionResult> ListOrder()
        {
            
            var orderList = await _orderService.ListOrder();
            return StatusCode(200, orderList);
            
        }
        [HttpDelete]
        [Route("api/order/{id}")]
        public IActionResult Delete(int id)
        {
            Orders order = _orderService.Get(id);
            if (order == null)
            {
                return NotFound("The Order record couldn't be found.");
            }
            order.IsDelete = true;
            _orderService.DeleteOrder(order);
            return StatusCode(200, "Order deleted sucessfully.");
        }
        [HttpPost]
        [Route("api/order")]
        public async Task<IActionResult> AddOrderList([FromBody] List<Orders> orderList)
        {
            int orderListCount = 0;
            int successAddCount = 0;
            int result = 0;
            // Loop to add order rows.
            foreach (Orders order in orderList)
            {
                orderListCount++;
                // Add order row.
                result = await _orderService.AddOrder(order);
                if (result != 0)
                {
                    successAddCount++;
                }
            }
            if (orderListCount == successAddCount)
            {
                // Return OK if all rows added sucessfully.
                return StatusCode(200, "Order List added sucessfully.");
            }
            // Return server error if added unsucessfully.
            return StatusCode(500, "For some reasons, this Order List can't be added.");
            
        }
    }
}