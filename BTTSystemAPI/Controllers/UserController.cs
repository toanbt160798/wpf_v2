﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BTTSystemAPI.Models;
using BTTSystemAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BTTSystemAPI.Controllers
{

    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        [HttpGet]
        [Route("api/users")]
        public IActionResult Get()
        {
            IEnumerable<Users> users = _userService.GetAll();
            return Ok(users);
        }
        [HttpGet]
        [Route("api/user/{id}")]
        public IActionResult Get(int id)
        {
            Users user = _userService.Get(id);

            if (user == null)
            {
                return NotFound("The User record couldn't be found.");
            }

            return Ok(user);
        }
        [HttpPost]
        [Route("api/user")]
        public IActionResult Add([FromBody] Users user)
        {
            if (user == null)
            {
                return BadRequest("User is null.");
            }            
            _userService.Add(user);
            return StatusCode(200, "User added sucessfully.");
        }
        
        [HttpPut]
        [Route("api/user/{id}")]
        public IActionResult Update(int id, [FromBody] Users user)
        {
            if (user == null)
            {
                return BadRequest("User is null.");
            }

            Users userToUpdate = user;
            if (userToUpdate == null)
            {
                return NotFound("The User record couldn't be found.");
            }
            else
            {
                user.UserId = id;
                _userService.Update(userToUpdate);
                return StatusCode(200, "User updated sucessfully.");
            }
        }
        [HttpPost]
        [Route("api/login")]
        public async Task<IActionResult> Login([FromBody] Users login)
        {
            // Check login username & password.
            Users user = await _userService.CheckLogin(login.UserName, login.Password);

            if (user != null)
            {
                return Ok(user.UserId);
            }
            else
            {
                return Unauthorized("Username or Password is not correct.");
            }            
        }
    }
}