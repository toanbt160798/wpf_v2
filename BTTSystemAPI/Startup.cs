﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection;
using BTTSystemAPI.Models;
using Microsoft.EntityFrameworkCore;
using BTTSystemAPI.Services.Interfaces;
using BTTSystemAPI.Services;
using BTTSystemAPI.Repositories.Interfaces;
using BTTSystemAPI.Repositories;

namespace BTTSystemAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder().
                            SetBasePath(env.ContentRootPath).
                            AddJsonFile("appsettings.json", false, true).
                            AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        public IConfiguration Configuration { get; }
        public static IContainer container { get; private set; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddDbContext<BTTSystemDBContext>(item => item.UseSqlServer(Configuration.GetConnectionString("SQLServer")));
            //services.AddEntityFrameworkNpgsql().AddDbContext<BTTSystemDBContext>(options =>
            //   options.UseNpgsql(Configuration.GetConnectionString("PosgreSQL")));

            services.AddScoped(typeof(IProductService), typeof(ProductService));
            services.AddScoped(typeof(IProductRepository), typeof(ProductRepository));
            services.AddScoped(typeof(IOrderService), typeof(OrderService));

            services.AddScoped(typeof(IOrderRepository), typeof(OrderRepository));
            services.AddScoped(typeof(IUserService), typeof(UserService));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAll",
                    p => p.AllowAnyOrigin().
                        AllowAnyHeader().
                        AllowAnyMethod().
                        AllowCredentials()
                        );
            });
            var builder = new ContainerBuilder();

            builder.Populate(services);

            container = builder.Build();
            return new AutofacServiceProvider(container);
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
                        IHostingEnvironment env,
                        ILoggerFactory loggerFctory,
                        IApplicationLifetime applicationLifetime)
        {
            app.UseCors("AllowAll");
            app.UseMvc();
            applicationLifetime.ApplicationStopped.Register(() => container.Dispose());
        }
    }
}
