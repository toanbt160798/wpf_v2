﻿using BTTSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services.Interfaces
{
    public interface IUserService
    {
        IEnumerable<Users> GetAll();
        Task<Users> CheckLogin(string username, string password);
        Users Get(int id);
        void Add(Users user);
        void Update(Users user);
    }
}
