﻿using BTTSystemAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services.Interfaces
{
    public interface IOrderService
    {
        Task<int> AddOrder(Orders order);
        Orders Get(int id);
        void DeleteOrder(Orders order);
        Task<List<Orders>> ListOrder();
    }
}
