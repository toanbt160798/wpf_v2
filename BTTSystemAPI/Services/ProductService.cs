﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using BTTSystemAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public void Add(Products product)
        {
            _productRepository.Add(product);
        }

        public void Delete(Products product)
        {
            _productRepository.Delete(product);
        }

        public Products Get(int id)
        {
            return _productRepository.Get(id);
        }

        public IEnumerable<Products> GetAll()
        {
            return _productRepository.GetAll();
        }

        public void Update(Products product)
        {
            _productRepository.Update(product);
        }
    }
}
