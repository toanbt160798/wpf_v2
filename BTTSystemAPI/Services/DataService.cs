﻿using BTTSystemAPI.Repositories.Interfaces;
using BTTSystemAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services
{
    public class DataService<TEntity> : IDataService<TEntity> where TEntity : class
    {
        private readonly IDataRepository<TEntity> _dataRepository;
        public DataService(IDataRepository<TEntity> dataRepository)
        {
            _dataRepository = dataRepository;
        }
        public void Add(TEntity entity)
        {
            _dataRepository.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _dataRepository.Delete(entity);
        }

        public TEntity Get(int id)
        {
            return _dataRepository.Get(id);
        }
        
        public void Update(TEntity entity)
        {
            _dataRepository.Update(entity);
        }
    }

}
