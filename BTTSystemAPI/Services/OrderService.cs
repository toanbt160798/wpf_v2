﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using BTTSystemAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        public OrderService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public Task<int> AddOrder(Orders order)
        {
            return _orderRepository.AddOrder(order);
        }
        public void DeleteOrder(Orders order)
        {
            _orderRepository.DeleteOrder(order);
        }

        public Orders Get(int id)
        {
            return _orderRepository.Get(id);
        }

        public Task<List<Orders>> ListOrder()
        {
            return _orderRepository.ListOrder();
        }
    }
}
