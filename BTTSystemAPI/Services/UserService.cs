﻿using BTTSystemAPI.Models;
using BTTSystemAPI.Repositories.Interfaces;
using BTTSystemAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BTTSystemAPI.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public IEnumerable<Users> GetAll()
        {
            return _userRepository.GetAll();
        }
        public Task<Users> CheckLogin(string username, string password)
        {
            return _userRepository.CheckLogin(username, password);
        }
        public Users Get(int id)
        {
            return _userRepository.Get(id);
        }

        public void Add(Users user)
        {
            _userRepository.Add(user);
        }
        public void Update(Users user)
        {
            _userRepository.Update(user);
        }
    }
}
