﻿using BTTSystemLivet.Views;
using Livet.Behaviors.Messaging;
using System;
using System.Windows;
using System.Windows.Controls;

namespace BTTSystemLivet.Messaging
{
    public class NavigationMessageAction : InteractionMessageAction<FrameworkElement>
    {
        public ContentControl NavigationHost
        {
            get { return (ContentControl)GetValue(NavigationHostProperty); }
            set { SetValue(NavigationHostProperty, value); }
        }

        public static readonly DependencyProperty NavigationHostProperty =
            DependencyProperty.Register("NavigationHost", typeof(ContentControl), typeof(NavigationMessageAction), new PropertyMetadata(null));


        protected override void InvokeAction(Livet.Messaging.InteractionMessage m)
        {
            if (!(m is NavigationMessage navigationMessage))
            {
                return;
            }

            if (NavigationHost is null)
            {
                throw new InvalidOperationException($"{nameof(NavigationHost)} is null.");
            }

            UserControl view = navigationMessage switch
            {
                { ViewName: "OrderView" } => new OrderView(),
                { ViewName: "HomeView" } => new HomeView(),
                { ViewName: "ProductView" } => new ProductView(),
                { ViewName: "AddProductView" } => new AddProductView(),
                { ViewName: "EditProductView" } => new EditProductView(),
                { ViewName: "AddOrderView" } => new AddOrderView(),
                _ => throw new InvalidOperationException($"{navigationMessage.ViewName} is invalid."),
            };
            if (navigationMessage.ViewModel != null)
            {
                view.DataContext = navigationMessage.ViewModel;
            }

            NavigationHost.Content = view;
        }

        protected override Freezable CreateInstanceCore()
        {
            return new NavigationMessageAction();
        }
    }
}
