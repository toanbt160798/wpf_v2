﻿using BTTSystemLivet.Models;
using Livet;
using Livet.EventListeners;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemLivet.ViewModels
{
    public class ValidationViewModel : ViewModel, INotifyDataErrorInfo
    {
        private ValidationModel Model;
        public ValidationViewModel(ValidationModel model)
        {
            if (model == null) throw new ArgumentNullException(nameof(model));

            Model = model;

            CompositeDisposable.Add(new PropertyChangedEventListener(Model)
            {
                (sender, e) => RaisePropertyChanged(e.PropertyName)
            });

            CompositeDisposable.Add(new EventListener<EventHandler<DataErrorsChangedEventArgs>>(
                h => Model.ErrorsChanged += h,
                h => Model.ErrorsChanged -= h,
                (sender, e) => ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(e.PropertyName))));
        }
        public ValidationViewModel()
        {
            Model = new ValidationModel();
        }
        public bool HasErrors
        {
            get { return Model.HasErrors; }
        }

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            return Model.GetErrors(propertyName);
        }
    }
}
