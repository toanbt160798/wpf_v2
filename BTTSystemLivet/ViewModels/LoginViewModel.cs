﻿using BTTSystemLivet.Services;
using BTTSystemLivet.Views;
using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace BTTSystemLivet.ViewModels
{
    public class LoginViewModel : ViewModel
    {
        private readonly UserService _userService = new UserService();
        public bool IsLogin { get; set; }
        private string _Username;
        private string _Password;
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }

        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    RaisePropertyChanged("Password");
                }
            }
        }
        private ListenerCommand<Window> _LoginCommand;

        public ListenerCommand<Window> LoginCommand
        {
            get
            {
                if (_LoginCommand == null)
                {
                    _LoginCommand = new ListenerCommand<Window>(Login);

                }
                return _LoginCommand;
            }
        }
        public ListenerCommand<PasswordBox> _PasswordChangedCommand;
        public ListenerCommand<PasswordBox> PasswordChangedCommand
        {
            get
            {
                return _PasswordChangedCommand = new ListenerCommand<PasswordBox>((p) => { Password = p.Password; });

            }
        }


        private async void Login(Window window)
        {

            HttpResponseMessage response = await _userService.Login(Username, Password);
            string responseData = response.Content.ReadAsStringAsync().Result;

            if (response.IsSuccessStatusCode)
            {
                int userId = int.Parse(responseData);
                Application.Current.Properties["userId"] = userId;
                IsLogin = true;
                window.Close();
            }
            else
            {
                IsLogin = false;
                MessageBox.Show("Username or Password not correct", "Error Message");
            }

        }
        public void Initialize()
        {
        }
    }
}
