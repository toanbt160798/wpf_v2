﻿using BTTSystemLivet.Messaging;
using BTTSystemLivet.Models;
using BTTSystemLivet.Services;
using Livet;
using Livet.Commands;
using Livet.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace BTTSystemLivet.ViewModels
{
    public class AddProductViewModel : ValidationViewModel
    {
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
        public void NavigateToProductView()
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = "ProductView" });
        }
        private readonly ProductService _productService = new ProductService();
        private Product Model;
        public AddProductViewModel(Product product) : base(product)
        {
            Model = product;
        }
        public AddProductViewModel()
        {
            Model = new Product();
            Product = new AddProductViewModel(Model);
        }
        public AddProductViewModel Product { get; }
        public string ProductName
        {
            get { return Model.ProductName; }
            set { Model.ProductName = value; }
        }
        public string Description
        {
            get { return Model.Description; }
            set { Model.Description = value; }
        }
        public string Price
        {
            get { return Model.Price; }
            set { Model.Price = value; }
        }
        public string Unit
        {
            get { return Model.Unit; }
            set { Model.Unit = value; }
        }
        #region Commands
        //Add product command
        private ViewModelCommand _AddProductCommand;
        public ViewModelCommand AddProductCommand
        {
            get
            {
                if (_AddProductCommand == null)
                {
                    _AddProductCommand = new ViewModelCommand(AddProduct);
                }
                return _AddProductCommand;
            }
        }
        //reset command
        private ViewModelCommand _ClearProductCommand;
        public ViewModelCommand ClearProductCommand
        {
            get
            {
                if (_ClearProductCommand == null)
                {
                    _ClearProductCommand = new ViewModelCommand(ClearProduct);
                }
                return _ClearProductCommand;
            }
        }

        #endregion
        #region Methods of product
        private async void AddProduct()
        {
            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Add this Product?", "Save Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                bool returnFlag = false;
                if (string.IsNullOrEmpty(ProductName))
                {
                    ProductName = "";
                    returnFlag = true;

                }
                if (string.IsNullOrEmpty(Unit))
                {
                    Unit = "";
                    returnFlag = true;
                }
                if (string.IsNullOrEmpty(Description))
                {
                    Description = "";
                    returnFlag = true;
                }
                if (string.IsNullOrEmpty(Price))
                {
                    Price = "";
                    returnFlag = true;
                }
                if (returnFlag)
                {
                    return;
                }
                else
                {
                    var product = new Product()
                    {
                        ProductName = ProductName,
                        Description = Description,
                        Price = Price,
                        Unit = Unit,
                        IsDelete = false,
                    };
                    var response = await _productService.AddProduct(product);
                    if (response.IsSuccessStatusCode)
                    {
                        NavigateToProductView();
                    }
                }
            }
            //ResetProduct();
        }

        private void ClearProduct()
        {
            // Clear all input fields.
            ProductName = "";
            Description = "";
            Price = "";
            Unit = "";
        }
        #endregion
        public void Initialize()
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
