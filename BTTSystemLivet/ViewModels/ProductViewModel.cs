﻿using BTTSystemLivet.Commands;
using BTTSystemLivet.Messaging;
using BTTSystemLivet.Models;
using BTTSystemLivet.Services;
using Livet;
using Livet.Commands;
using Livet.Messaging;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace BTTSystemLivet.ViewModels
{
    public class ProductViewModel : ViewModel
    {
        private readonly ProductService _productService = new ProductService();
        private readonly OrderService _orderService = new OrderService();
        private ObservableCollection<Product> _listProduct;
        public ObservableCollection<Product> ListProduct
        {
            get
            {
                return _listProduct;
            }
            set
            {
                if (_listProduct != value)
                {
                    _listProduct = value;
                    RaisePropertyChanged("ListProduct");
                }
            }
        }
        #region Commands
        private ListenerCommand<int> _ShowEditProductCommand;

        public ListenerCommand<int> ShowEditProductCommand
        {
            get
            {
                if (_ShowEditProductCommand == null)
                {
                    _ShowEditProductCommand = new ListenerCommand<int>(ShowEditProductView);
                }
                return _ShowEditProductCommand;
            }
        }

        public void ShowEditProductView(int productId)
        {
            App.Current.Properties["productId"] = productId;
            Navigate("EditProductView");
        }
        private ListenerCommand<int> _DeleteProductCommand;
        public ListenerCommand<int> DeleteProductCommand
        {
            get
            {
                if (_DeleteProductCommand == null)
                {
                    _DeleteProductCommand = new ListenerCommand<int>(DeleteProduct);
                }
                return _DeleteProductCommand;
            }
        }
        // PageList command
        public ICommand PreviousCommand { get; private set; }
        public ICommand NextCommand { get; private set; }
        public ICommand FirstCommand { get; private set; }
        public ICommand LastCommand { get; private set; }
        #endregion
        #region Fields And Properties
        private readonly int itemPerPage = 10;
        private readonly int itemcount;
        private int _currentPageIndex;
        public int CurrentPageIndex
        {
            get { return _currentPageIndex; }
            private set
            {
                _currentPageIndex = value;
                RaisePropertyChanged("CurrentPage");
            }
        }
        public int CurrentPage
        {
            get { return _currentPageIndex + 1; }
        }
        private int _totalPage;
        public int TotalPage
        {
            get { return _totalPage; }
            private set
            {
                _totalPage = value;
                RaisePropertyChanged("TotalPage");
            }
        }
        private CollectionViewSource _ViewList;
        public CollectionViewSource ViewList
        {
            get
            {
                return _ViewList;
            }
            set
            {
                if (_ViewList != value)
                {
                    _ViewList = value;
                    RaisePropertyChanged("ViewList");
                }
            }
        }
        #endregion
        #region Pagination Methods
        public void ShowNextPage()
        {
            CurrentPageIndex++;
            ViewList.View.Refresh();
        }

        public void ShowPreviousPage()
        {
            CurrentPageIndex--;
            ViewList.View.Refresh();
        }

        public void ShowFirstPage()
        {
            CurrentPageIndex = 0;
            ViewList.View.Refresh();
        }

        public void ShowLastPage()
        {
            CurrentPageIndex = TotalPage - 1;
            ViewList.View.Refresh();
        }
        private void ViewFilter(object sender, FilterEventArgs e)
        {
            var products = ListProduct.ToArray();
            Product product = e.Item as Product;
            int index = Array.IndexOf(products, product);
            if (product != null)
            {
                if (index >= itemPerPage * CurrentPageIndex && index < itemPerPage * (CurrentPageIndex + 1))
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                }
            }
        }
        private void CalculateTotalPages()
        {
            if (itemcount % itemPerPage == 0)
            {
                TotalPage = (itemcount / itemPerPage);
            }
            else
            {
                TotalPage = (itemcount / itemPerPage) + 1;
            }
        }
        #endregion
        public void Initialize()
        {

        }
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
        public void NavigateToProductView()
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = "ProductView" });
        }
        public ProductViewModel()
        {
            GetProducts();
            ViewList = new CollectionViewSource
            {
                Source = ListProduct
            };
            ViewList.Filter += new FilterEventHandler(ViewFilter);
            CurrentPageIndex = 0;
            itemcount = ListProduct.Count;
            CalculateTotalPages();

            NextCommand = new NextPageCommand(this);
            PreviousCommand = new PreviousPageCommand(this);
            FirstCommand = new FirstPageCommand(this);
            LastCommand = new LastPageCommand(this);

        }
        //}
        private async void GetProducts()
        {
            ListProduct = new ObservableCollection<Product>(await _productService.ListProduct());

        }
        private async void DeleteProduct(int productId)
        {
            App.Current.Properties["productId"] = productId;
            var product = await _productService.GetProduct(productId);
            var order = await _orderService.ListOrder();
            var result = order.Where(o => o.ProductId == productId).FirstOrDefault();
            if (result == null)
            {
                if (product != null)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Product?", "Delete Confirmation", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        HttpResponseMessage response = await _productService.DeleteProduct(productId);

                        if (response.IsSuccessStatusCode)
                        {
                            GetProducts();
                            ViewList.Source = ListProduct;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("This record is being used in another place!", "Error Message");
            }
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
