﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using BTTSystemLivet.Commands;
using BTTSystemLivet.Messaging;
using BTTSystemLivet.Models;
using BTTSystemLivet.Services;
using Livet;
using Livet.Commands;
using Livet.Messaging;

namespace BTTSystemLivet.ViewModels
{
    public class OrderViewModel : ViewModel
    {
        public ObservableCollection<Order> _orderList;
        public ObservableCollection<Order> _orders;
        public static List<Product> _productList;
        public static int? _AllQuantity;
        public static decimal? _AllPrice;
        private readonly OrderService _orderService = new OrderService();
        private readonly ProductService _productService = new ProductService();
        public Order _selectedItem;
        public Order SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }
        public ObservableCollection<Order> OrderList
        {
            get
            {
                return _orderList;
            }
            set
            {
                if (_orderList != value)
                {
                    _orderList = value;
                    RaisePropertyChanged("OrderList");
                }
            }
        }
        public ObservableCollection<Order> Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                if (_orders != value)
                {
                    _orders = value;
                    RaisePropertyChanged("Orders");
                }
            }
        }
        public List<Product> ProductList
        {
            get
            {
                return _productList;
            }
            set
            {
                if (_productList != value)
                {
                    _productList = value;
                    RaisePropertyChanged("ProductList");
                }
            }
        }
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
        private async void SetupOrderDataGrid()
        {
            // Create 1 new row of order list.
            OrderList = new ObservableCollection<Order>(await _orderService.ListOrder());
            Orders = new ObservableCollection<Order>
            {
                new Order()
            };
            // Get product list data from the database.
            ProductList = await _productService.ListProduct();
        }
        private ListenerCommand<int> _DeleteOrderCommand;
        public ListenerCommand<int> DeleteOrderCommand
        {
            get
            {
                if (_DeleteOrderCommand == null)
                {
                    _DeleteOrderCommand = new ListenerCommand<int>(DeleteOrder);
                }
                return _DeleteOrderCommand;
            }
        }

        private async void DeleteOrder(int orderId)
        {
            App.Current.Properties["orderId"] = orderId;
            var orders = await _orderService.ListOrder();
            var result = orders.Where(o => o.OrderId == orderId).FirstOrDefault();
            if (result != null)
            {
                if (orders != null)
                {
                    MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Delete this Order?", "Delete Confirmation", MessageBoxButton.YesNo);
                    if (messageBoxResult == MessageBoxResult.Yes)
                    {
                        HttpResponseMessage response = await _orderService.DeleteOrder(orderId);

                        if (response.IsSuccessStatusCode)
                        {
                            SetupOrderDataGrid();
                            ViewList.Source = OrderList;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("This record is being used in another place!", "Message error");
            }
        }

        private ViewModelCommand _DeleteRowCommand;

        public ViewModelCommand DeleteRowCommand
        {
            get
            {
                if (_DeleteRowCommand == null)
                {
                    _DeleteRowCommand = new ViewModelCommand(DeleteRow);
                }
                return _DeleteRowCommand;
            }
        }

        private ViewModelCommand _AddRowCommand;

        public ViewModelCommand AddRowCommand
        {
            get
            {
                if (_AddRowCommand == null)
                {
                    _AddRowCommand = new ViewModelCommand(AddRow);
                }
                return _AddRowCommand;
            }
        }
        private void DeleteRow()
        {
            Orders.Remove(SelectedItem);
        }
        private void AddRow()
        {
            Orders.Add(new Order());
        }
        #region PageList command
        public ICommand PreviousCommand { get; private set; }
        public ICommand NextCommand { get; private set; }
        public ICommand FirstCommand { get; private set; }
        public ICommand LastCommand { get; private set; }
        #endregion
        #region Fields And Properties
        private readonly int itemPerPage = 10;
        private readonly int itemcount;
        private int _currentPageIndex;
        public int CurrentPageIndex
        {
            get { return _currentPageIndex; }
            private set
            {
                _currentPageIndex = value;
                RaisePropertyChanged("CurrentPage");
            }
        }
        public int CurrentPage
        {
            get { return _currentPageIndex + 1; }
        }

        private int _totalPage;
        public int TotalPage
        {
            get { return _totalPage; }
            private set
            {
                _totalPage = value;
                RaisePropertyChanged("TotalPage");
            }
        }
        private CollectionViewSource _ViewList;
        public CollectionViewSource ViewList
        {
            get
            {
                return _ViewList;
            }
            set
            {
                if (_ViewList != value)
                {
                    _ViewList = value;
                    RaisePropertyChanged("ViewList");
                }
            }
        }
        #endregion
        #region Pagination Methods
        public void ShowNextPage()
        {
            CurrentPageIndex++;
            ViewList.View.Refresh();
        }

        public void ShowPreviousPage()
        {
            CurrentPageIndex--;
            ViewList.View.Refresh();
        }

        public void ShowFirstPage()
        {
            CurrentPageIndex = 0;
            ViewList.View.Refresh();
        }

        public void ShowLastPage()
        {
            CurrentPageIndex = TotalPage - 1;
            ViewList.View.Refresh();
        }

        private void ViewFilter(object sender, FilterEventArgs e)
        {
            var orders = OrderList.ToArray();
            Order order = e.Item as Order;
            int index = Array.IndexOf(orders, order);
            if (order != null)
            {
                if (index >= itemPerPage * CurrentPageIndex && index < itemPerPage * (CurrentPageIndex + 1))
                {
                    e.Accepted = true;
                }
                else
                {
                    e.Accepted = false;
                }
            }
        }

        private void CalculateTotalPages()
        {
            if (itemcount % itemPerPage == 0)
            {
                TotalPage = (itemcount / itemPerPage);
            }
            else
            {
                TotalPage = (itemcount / itemPerPage) + 1;
            }
        }
        #endregion
        public void Initialize()
        {

        }
        public OrderViewModel()
        {
            SetupOrderDataGrid();
            ViewList = new CollectionViewSource
            {
                Source = OrderList
            };
            ViewList.Filter += new FilterEventHandler(ViewFilter);
            CurrentPageIndex = 0;
            itemcount = OrderList.Count;
            CalculateTotalPages();

            NextCommand = new NextPageCommand(this);
            PreviousCommand = new PreviousPageCommand(this);
            FirstCommand = new FirstPageCommand(this);
            LastCommand = new LastPageCommand(this);
        }
    }
}