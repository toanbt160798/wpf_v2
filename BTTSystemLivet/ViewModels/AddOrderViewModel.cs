﻿using BTTSystemLivet.Messaging;
using BTTSystemLivet.Models;
using BTTSystemLivet.Services;
using Livet.Commands;
using Livet.Messaging;
using System.Windows;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace BTTSystemLivet.ViewModels
{
    public class AddOrderViewModel : ValidationViewModel
    {
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
        public void NavigateToProductView()
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = "OrderView" });
        }
        public ObservableCollection<Order> _orderList;
        public ObservableCollection<Order> _orders;
        public static List<Product> _productList;
        private readonly ProductService _productService = new ProductService();
        private readonly OrderService _orderService = new OrderService();
        public Order _selectedItem;
        public Order SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged("SelectedItem");
            }
        }
        public ObservableCollection<Order> OrderList
        {
            get
            {
                return _orderList;
            }
            set
            {
                if (_orderList != value)
                {
                    _orderList = value;
                    RaisePropertyChanged("OrderList");
                }
            }
        }
        public ObservableCollection<Order> Orders
        {
            get
            {
                return _orders;
            }
            set
            {
                if (_orders != value)
                {
                    _orders = value;
                    RaisePropertyChanged("Orders");
                }
            }
        }
        public List<Product> ProductList
        {
            get
            {
                return _productList;
            }
            set
            {
                if (_productList != value)
                {
                    _productList = value;
                    RaisePropertyChanged("ProductList");
                }
            }
        }
        private Order Model;
        public AddOrderViewModel(Order order) : base(order)
        {
            Model = order;
        }
        public AddOrderViewModel()
        {
            SetupOrderDataGrid();
            Model = new Order();
            Order = new AddOrderViewModel(Model);
        }
        public AddOrderViewModel Order { get; }
        public string ProductName
        {
            get { return Model.ProductName; }
            set { Model.ProductName = value; }
        }
        public string Quantity
        {
            get { return Model.Quantity; }
            set { Model.Quantity = value; }
        }
        public void Initialize()
        {

        }
        private async void SetupOrderDataGrid()
        {
            // Create 1 new row of order list.
            OrderList = new ObservableCollection<Order>(await _orderService.ListOrder());
            Orders = new ObservableCollection<Order>
            {
                new Order()
            };
            // Get product list data from the database.
            ProductList = await _productService.ListProduct();
        }
        private ViewModelCommand _DeleteRowCommand;

        public ViewModelCommand DeleteRowCommand
        {
            get
            {
                if (_DeleteRowCommand == null)
                {
                    _DeleteRowCommand = new ViewModelCommand(DeleteRow);
                }
                return _DeleteRowCommand;
            }
        }

        private ViewModelCommand _AddRowCommand;

        public ViewModelCommand AddRowCommand
        {
            get
            {
                if (_AddRowCommand == null)
                {
                    _AddRowCommand = new ViewModelCommand(AddRow);
                }
                return _AddRowCommand;
            }
        }
        private ViewModelCommand _SaveOrderCommand;

        public ViewModelCommand SaveOrderCommand
        {
            get
            {
                if (_SaveOrderCommand == null)
                {
                    _SaveOrderCommand = new ViewModelCommand(AddOrderList);
                }
                return _SaveOrderCommand;
            }
        }
        private async void AddOrderList()
        {

            MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to submit these orders?", "Submit Confirmation", MessageBoxButton.YesNo);
            if (messageBoxResult == MessageBoxResult.Yes)
            {
                // Get current user id from App.Current.Properties.
                int userId = int.Parse(Application.Current.Properties["userId"].ToString());
                List<Order> submitOrderList = new List<Order>();
                //bool returnFlag = false;
                Order submitOrder;
                foreach (Order order in _orders)
                {
                    submitOrder = new Order();
                    if (order.ProductId == 0)
                    {
                        //this.ProductName = "";
                        //returnFlag = true;
                        MessageBox.Show("Please choose a product.", "Error Message");
                        return;
                    }
                    if (order.Quantity == null || order.Quantity == "")
                    {
                        MessageBox.Show("Quantity must not be empty.", "Error Message");
                        //this.Quantity = "";
                        //returnFlag = true;
                        return;
                    }
                    //if (returnFlag)
                    //{
                    //    return;
                    //}
                    submitOrder = order;
                    submitOrder.IsDelete = false;
                    submitOrder.UserId = userId;
                    submitOrderList.Add(submitOrder);
                }
                // Function call the API to add Product and get response data.
                HttpResponseMessage response = await _orderService.AddOrderList(submitOrderList);
                //var responseData = response.Content.ReadAsStringAsync().Result;
                if (response.IsSuccessStatusCode)
                {
                    NavigateToProductView();
                }
            }
        }
        private void DeleteRow()
        {
            Orders.Remove(SelectedItem);
        }
        private void AddRow()
        {
            Orders.Add(new Order());
        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
