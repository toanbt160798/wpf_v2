﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

using Livet;
using Livet.Commands;
using Livet.Messaging;
using Livet.Messaging.IO;
using Livet.EventListeners;
using Livet.Messaging.Windows;

using BTTSystemLivet.Models;
using BTTSystemLivet.Views;
using System.Windows;
using BTTSystemLivet.Messaging;
using System.Windows.Data;
using System.Collections.ObjectModel;
using BTTSystemLivet.Services;
using BTTSystemLivet.Commands;
using System.Windows.Input;

namespace BTTSystemLivet.ViewModels
{
    public class MainWindowViewModel : ViewModel
    {
        public bool Isloaded = false;
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;
        public ListenerCommand<Window> _LoadedWindowCommand;
        public ListenerCommand<Window> LoadedWindowCommand
        {
            get
            {
                if (_LoadedWindowCommand == null)
                {
                    _LoadedWindowCommand = new ListenerCommand<Window>((p) =>
                    {
                        Isloaded = true;
                        if (p == null)
                            return;
                        p.Hide();

                        LoginView loginWindow = new LoginView();
                        loginWindow.ShowDialog();
                        if (loginWindow.DataContext == null)
                            return;
                        var loginVM = loginWindow.DataContext as LoginViewModel;
                        if (loginVM.IsLogin)
                        {
                            p.Show();
                        }
                        else
                        {
                            p.Close();
                        }
                    });
                }
                return _LoadedWindowCommand;
            }
        }
        public void Initialize()
        {
            Navigate("HomeView");
        }
        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
    }
}
