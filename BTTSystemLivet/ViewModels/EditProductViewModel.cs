﻿using BTTSystemLivet.Messaging;
using BTTSystemLivet.Models;
using BTTSystemLivet.Services;
using Livet.Commands;
using Livet.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace BTTSystemLivet.ViewModels
{
    public class EditProductViewModel : ValidationViewModel
    {
        private readonly InteractionMessenger _navigationMessenger = NavigationMessenger.Messenger;

        public void Navigate(string viewName)
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = viewName });
        }
        public void NavigateToProductView()
        {
            _navigationMessenger.Raise(new NavigationMessage { ViewName = "ProductView" });
        }
        private readonly ProductService _productService = new ProductService();
        private Product _Product;
        public Product Product
        {
            get
            {
                return _Product;
            }
            set
            {
                if (_Product != value)
                {
                    _Product = value;
                    RaisePropertyChanged("Product");
                }
            }
        }
        public EditProductViewModel(Product product) : base(product)
        {
            Product = product;
        }
        public EditProductViewModel()
        {
            GetProduct();
            EditProduct = new EditProductViewModel(Product);
        }
        public EditProductViewModel EditProduct { get; }
        private async void GetProduct()
        {
            int productId = int.Parse(App.Current.Properties["productId"].ToString());

            // Get product by productId.
            Product = await _productService.GetProduct(productId);
        }
        public int ProductId
        {
            get { return Product.ProductId; }
            set { Product.ProductId = value; }
        }
        public string ProductName
        {
            get { return Product.ProductName; }
            set { Product.ProductName = value; }
        }
        public string Description
        {
            get { return Product.Description; }
            set { Product.Description = value; }
        }
        public string Price
        {
            get { return Product.Price; }
            set { Product.Price = value; }
        }
        public string Unit
        {
            get { return Product.Unit; }
            set { Product.Unit = value; }
        }
        #region Commands
        //Update command
        private ViewModelCommand _UpdateProductCommand;
        public ViewModelCommand UpdateProductCommand
        {
            get
            {
                if (_UpdateProductCommand == null)
                {
                    _UpdateProductCommand = new ViewModelCommand(UpdateProduct);
                }
                return _UpdateProductCommand;
            }
        }
        //reset command
        private ViewModelCommand _ResetProductCommand;
        public ViewModelCommand ResetProductCommand
        {
            get
            {
                if (_ResetProductCommand == null)
                {
                    _ResetProductCommand = new ViewModelCommand(ResetProduct);
                }
                return _ResetProductCommand;
            }
        }
        //Clear command
        private ViewModelCommand _ClearProductCommand;
        public ViewModelCommand ClearProductCommand
        {
            get
            {
                if (_ClearProductCommand == null)
                {
                    _ClearProductCommand = new ViewModelCommand(ClearProduct);
                }
                return _ClearProductCommand;
            }
        }

        #endregion
        #region Methods of product
        private async void UpdateProduct()
        {
            Product product = await _productService.GetProduct(ProductId);
            if (product != null)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Are you sure you want to Update this Product?", "Save Confirmation", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes)
                {
                    bool returnFlag = false;
                    if (string.IsNullOrEmpty(ProductName))
                    {
                        ProductName = "";
                        returnFlag = true;

                    }
                    if (string.IsNullOrEmpty(Unit))
                    {
                        Unit = "";
                        returnFlag = true;
                    }
                    if (string.IsNullOrEmpty(Description))
                    {
                        Description = "";
                        returnFlag = true;
                    }
                    if (string.IsNullOrEmpty(Price))
                    {
                        Price = "";
                        returnFlag = true;
                    }
                    if (returnFlag)
                    {
                        return;
                    }
                    else
                    {
                        product.ProductName = ProductName;
                        product.Description = Description;
                        product.Price = Price;
                        product.Unit = Unit;
                        product.IsDelete = false;
                        var response = await _productService.EditProduct(product);
                        if (response.IsSuccessStatusCode)
                        {
                            NavigateToProductView();
                        }
                    }
                }
            }
        }

        private void ClearProduct()
        {
            // Clear all input fields.
            ProductName = "";
            Description = "";
            Price = "";
            Unit = "";
        }
        private async void ResetProduct()
        {
            int productId = int.Parse(App.Current.Properties["productId"].ToString());
            // Get product by productId.
            var ProductRe = await _productService.GetProduct(productId);
            ProductName = ProductRe.ProductName;
            Description = ProductRe.Description;
            Price = ProductRe.Price;
            Unit = ProductRe.Unit;
        }
        #endregion
        public void Initialize()
        {

        }
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }
    }
}
