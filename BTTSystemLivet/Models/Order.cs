﻿using BTTSystemLivet.Services;
using Livet;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemLivet.Models
{
    public class Order : ValidationModel
    {
        private readonly ProductService _productService = new ProductService();
        private readonly UserService _userService = new UserService();
        int _ProductId;
        int _UserId;
        string _Description;
        string _ProductName;
        string _Username;
        string _Unit;
        string _Price;
        string _Quantity;
        string _TotalPrice;
        bool? _IsDelete;
        public int OrderId { get; set; }
        public virtual Product Product { get; set; }
        public virtual User User { get; set; }
        public bool? IsDelete
        {
            get { return _IsDelete; }
            set
            {
                _IsDelete = value;
                RaisePropertyChanged("IsDelete");
            }
        }
        public int ProductId
        {
            get
            {
                return _ProductId;
            }
            set
            {
                if (_ProductId != value)
                {
                    _ProductId = value;
                    RaisePropertyChanged("ProductId");
                    Validation(value);
                    RefreshData(_ProductId);
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }
        public int UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                if (_UserId != value)
                {
                    _UserId = value;
                    RaisePropertyChanged("UserId");
                    RefreshDataUser(_UserId);
                }
            }
        }
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description != value)
                {
                    _Description = value;
                    RaisePropertyChanged("Description");
                }
            }
        }
        [Required(ErrorMessage = "Product name must not be empty.")]
        public string ProductName
        {
            get
            {
                return _ProductName;
            }
            set
            {
                if (_ProductName != value)
                {
                    _ProductName = value;
                    RaisePropertyChanged("ProductName");
                    Validation(value);
                }
            }
        }
        public string Username
        {
            get
            {
                return _Username;
            }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    RaisePropertyChanged("Username");
                }
            }
        }
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit != value)
                {
                    _Unit = value;
                    RaisePropertyChanged("Unit");
                }
            }
        }
        public string Price
        {
            get
            {
                return _Price;
            }
            set
            {
                if (_Price != value)
                {
                    _Price = value;
                    RaisePropertyChanged("Price");
                }
            }
        }
        [Required]
        public string Quantity
        {
            get
            {
                return _Quantity;
            }
            set
            {
                if (_Quantity == value)
                    return;
                _Quantity = value;
                RaisePropertyChanged("Quantity");
                ValidationQuantity();
                CalculateTotalPrice(_Price, _Quantity);
            }
        }
        public string TotalPrice
        {
            get
            {
                return _TotalPrice;
            }
            set
            {
                if (_TotalPrice != value)
                {
                    _TotalPrice = value;
                    RaisePropertyChanged("TotalPrice");
                    CalculateTotalPrice(_Price, _Quantity);
                }
            }
        }
        // Private Methods
        private void ValidationQuantity()
        {
            string[] message;
            bool error = false;
            string validationQuantity = null;
            if (!string.IsNullOrEmpty(_Quantity))
            {
                if (!Int32.TryParse(_Quantity, out _))
                {
                    validationQuantity = "Quantity is not a whole number";
                    error = true;
                }
                else if (int.Parse(_Quantity) <= 0)
                {
                    validationQuantity = "Quantity must be greater than 0.";
                    error = true;
                }
            }
            else if (_Quantity == "" || _Quantity == null)
            {
                validationQuantity = "Quantity must not be empty.";
                error = true;
            }
            if (error)
            {
                message = new[] { validationQuantity };
            }
            else
            {
                message = null;
            }
            UpdateErrors(message, nameof(Quantity));
        }
        private async void RefreshData(int _ProductId)
        {
            // Function call the API to get Product.
            Product product = await _productService.GetProduct(_ProductId);
            this.Description = (product == null ? "" : product.Description);
            this.Unit = (product == null ? "" : product.Unit);
            this.Price = (product == null ? "" : product.Price);
            this.ProductName = (product == null ? "" : product.ProductName);
        }
        private async void RefreshDataUser(int _UserId)
        {
            User user = await _userService.GetUser(_UserId);
            this.Username = (user == null ? "" : user.Username);
        }
        private void CalculateTotalPrice(string _Price, string _Quantity)
        {
            string formatPrice = "0";
            if (_Price != null)
            {
                formatPrice = _Price.Replace(".", ",");
            }
            if (_Quantity == null || _Quantity == "" || !Int32.TryParse(_Quantity, out _))
            {
                this.TotalPrice = "0";
            }
            else
            {
                decimal result = decimal.Parse(formatPrice) * int.Parse(_Quantity);
                this.TotalPrice = result.ToString().Replace(",", ".");
            }
        }
    }
}
