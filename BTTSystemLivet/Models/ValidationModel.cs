﻿using Livet;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemLivet.Models
{
    public class ValidationModel : NotificationObject, INotifyDataErrorInfo
    {
        protected Dictionary<string, string[]> Errors;

        public ValidationModel()
        {
            Errors = new Dictionary<string, string[]>();
            HasErrors = false;
        }
        private bool _HasErrors;
        public bool HasErrors
        {
            get
            { return _HasErrors; }
            private set
            {
                if (_HasErrors == value)
                    return;
                _HasErrors = value;
                RaisePropertyChanged();
            }
        }
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        protected virtual void RaiseErrorsChanged([CallerMemberName]string propertyName = "")
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }
        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrWhiteSpace(propertyName)) return null;
            if (!Errors.ContainsKey(propertyName)) return null;
            return Errors[propertyName];
        }
        protected void Validation(object value, [CallerMemberName] string propertyName = "")
        {
            if (string.IsNullOrWhiteSpace(propertyName)) return;

            var results = new List<ValidationResult>();
            string[] messages = null;

            if (!Validator.TryValidateProperty(
                value,
                new ValidationContext(this, null, null) { MemberName = propertyName },
                results))
            {
                messages = results.Select(x => x.ErrorMessage).ToArray();
            }

            UpdateErrors(messages, propertyName);
        }
        protected void UpdateErrors(string[] messages, [CallerMemberName] string propertyName = "")
        {
            if (string.IsNullOrWhiteSpace(propertyName)) return;

            Errors[propertyName] = messages;

            HasErrors = Errors.Values.Any(x => x != null);
            RaiseErrorsChanged(propertyName);
        }
    }
}
