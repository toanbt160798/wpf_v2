﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
namespace BTTSystemLivet.Models
{
    public class Product : ValidationModel
    {
        public Product()
        {
            Order = new HashSet<Order>();
        }

        public int ProductId { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Order> Order { get; set; }
        private string _ProductName;
        [Display(Name = "Product Name")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Product name must not longer than 50 characters")]
        [Required(ErrorMessage = "Product name must not be empty.")]

        public string ProductName
        {
            get
            {
                return _ProductName;
            }
            set
            {
                if (_ProductName == value)
                    return;
                _ProductName = value;
                RaisePropertyChanged("ProductName");
                Validation(value);
            }
        }
        private string _Description;
        [Required(ErrorMessage = "Description must not be empty")]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description == value)
                    return;

                _Description = value;
                RaisePropertyChanged("Description");
                Validation(value);
            }
        }

        private string _Unit;
        [StringLength(10, ErrorMessage = "Unit must not longer than 10 characters")]
        [Required(ErrorMessage = "Unit must not be empty.")]
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit == value)
                    return;

                _Unit = value;
                RaisePropertyChanged("Unit");
                Validation(value);
            }
        }

        private string _Price;
        [Required]
        public string Price
        {
            get
            {
                return _Price;

            }
            set
            {
                if (_Price == value)
                    return;
                _Price = value;
                RaisePropertyChanged("Price");
                ValidationPrice();
            }
        }
        private void ValidationPrice()
        {
            string[] message;
            bool error = false;
            string validationPrice = null;
            if (!string.IsNullOrEmpty(_Price))
            {
                if (!Double.TryParse(_Price, out _) || _Price.Contains(","))
                {
                    validationPrice = "Price is not a whole number";
                    error = true;
                }
                else if (decimal.Parse(_Price) <= 0)
                {
                    validationPrice = "Price must be greater than 0.";
                    error = true;
                }
            }
            else if (_Price == "" || _Price == null)
            {
                validationPrice = "Price must not be empty.";
                error = true;
            }
            if (error)
            {
                message = new[] { validationPrice };
            }
            else
            {
                message = null;
            }
            UpdateErrors(message, nameof(Price));
        }
    }
}
