﻿using Livet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTTSystemLivet.Models
{
    public class User : ViewModel
    {
        public User()
        {
            Order = new HashSet<Order>();
        }

        public int UserId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Order> Order { get; set; }
    }
}
