﻿using BTTSystemLivet.ViewModels;
using Livet;
using Livet.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BTTSystemLivet.Commands
{
    class NextPageCommand : ICommand
    {
        private ProductViewModel productViewModel;
        private OrderViewModel orderViewModel;
        public NextPageCommand(ProductViewModel productViewModel)
        {
            this.productViewModel = productViewModel;
        }

        public NextPageCommand(OrderViewModel orderViewModel)
        {
            this.orderViewModel = orderViewModel;
        }
        public bool CanExecute(object parameter)
        {
            bool result = false;
            if (productViewModel != null)
            {
                result = productViewModel.TotalPage - 1 > productViewModel.CurrentPageIndex;
            }
            if (orderViewModel != null)
            {
                result = orderViewModel.TotalPage - 1 > orderViewModel.CurrentPageIndex;
            }
            return result;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (productViewModel != null)
            {
                productViewModel.ShowNextPage();
            }
            if (orderViewModel != null)
            {
                orderViewModel.ShowNextPage();
            }
        }
    }
}
