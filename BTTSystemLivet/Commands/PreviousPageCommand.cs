﻿using BTTSystemLivet.ViewModels;
using Livet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BTTSystemLivet.Commands
{
    class PreviousPageCommand : ICommand
    {
        private MainWindowViewModel viewModel;
        private ProductViewModel productViewModel;
        private OrderViewModel orderViewModel;

        public PreviousPageCommand(MainWindowViewModel viewModel)
        {
            this.viewModel = viewModel;
        }

        public PreviousPageCommand(ProductViewModel productViewModel)
        {
            this.productViewModel = productViewModel;
        }

        public PreviousPageCommand(OrderViewModel orderViewModel)
        {
            this.orderViewModel = orderViewModel;
        }

        public bool CanExecute(object parameter)
        {
            bool result = false;
            if (productViewModel != null)
            {
                result = productViewModel.CurrentPageIndex != 0;
            }
            if (orderViewModel != null)
            {
                result = orderViewModel.CurrentPageIndex != 0;
            }
            return result;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (productViewModel != null)
            {
                productViewModel.ShowPreviousPage();
            }
            if (orderViewModel != null)
            {
                orderViewModel.ShowPreviousPage();
            }
        }
    }
}
