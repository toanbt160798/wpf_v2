﻿using BTTSystemLivet.ViewModels;
using Livet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BTTSystemLivet.Commands
{
    class FirstPageCommand : ICommand
    {
        private ProductViewModel productViewModel;
        private OrderViewModel orderViewModel;

        public FirstPageCommand(ProductViewModel productViewModel)
        {
            this.productViewModel = productViewModel;
        }

        public FirstPageCommand(OrderViewModel orderViewModel)
        {
            this.orderViewModel = orderViewModel;
        }

        public bool CanExecute(object parameter)
        {
            if (productViewModel != null)
            {
                if (productViewModel.CurrentPageIndex == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            if (orderViewModel != null)
            {
                if (orderViewModel.CurrentPageIndex == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (productViewModel != null)
            {
                productViewModel.ShowFirstPage();
            }
            if (orderViewModel != null)
            {
                orderViewModel.ShowFirstPage();
            }
        }
    }
}
