﻿using BTTSystemLivet.ViewModels;
using Livet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BTTSystemLivet.Commands
{
    class LastPageCommand : ICommand
    {
        private ProductViewModel productViewModel;
        private OrderViewModel orderViewModel;

        public LastPageCommand(ProductViewModel productViewModel)
        {
            this.productViewModel = productViewModel;
        }

        public LastPageCommand(OrderViewModel orderViewModel)
        {
            this.orderViewModel = orderViewModel;
        }

        public bool CanExecute(object parameter)
        {
            bool result = false;
            if (productViewModel != null)
            {
                result = productViewModel.CurrentPage != productViewModel.TotalPage;
            }
            if (orderViewModel != null)
            {
                result = orderViewModel.CurrentPage != orderViewModel.TotalPage;
            }
            return result;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (productViewModel != null)
            {
                productViewModel.ShowLastPage();
            }
            if (orderViewModel != null)
            {
                orderViewModel.ShowLastPage();
            }
        }
    }
}
